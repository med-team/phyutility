Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: phyutility
Upstream-Contact: Stephen Smith <blackrim@gmail.com>
Source: https://github.com/blackrim/phyutility/
Files-Excluded: */*.so

Files: *
Copyright: © 2007-2014 Stephen Smith <blackrim@gmail.com>
License: GPL-3.0+

Files: debian/*
Copyright: © 2012-2014 Andreas Tille <tille@debian.org>
           © 2014 Stephen Smith <blackrim@gmail.com>
License: GPL-3.0+

Files: src/jebl/*
Copyright: © 2002-2006 Marc A. Suchard
           © 2002-2006 Andrew Rambaut
           © 2002-2006 Alexei Drummond
           © 2002-2006 Korbinian Strimmer
           © 2002-2006 Matthew Goode
           © 2002-2006 Joseph Heled
           © 2002-2006 Matt Kearse
           © 2002-2006 Tobias Thierer
           © 2002-2006 Stephen Smith <blackrim@gmail.com>
License: LGPL-2.1

Files: src/phyutility/drb/*
Copyright: © 2004-2014 The Apache Software Foundation
License: Apache-2.0

Files: src/jade/data/*
Copyright: © 1999-2001 PAL Development Core Team
License: LGPL-2.1

Files: src/org/virion/*
Copyright: © 2002-2005 BEAST Development Core Team
License: LGPL-2.1

Files: src/org/xml/*
Copyright: © David Megginson
License: other
 Public domain

License: GPL-3.0+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in
 "/usr/share/common-licenses/GPL-3".

License: LGPL-2.1
 This library is free software; you can redistribute it and/or modify
 it under the terms of the GNU Lesser General Public License as
 published by the Free Software Foundation; either version 2.1 of the
 License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 MA 02110-1301 USA
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in
 "/usr/share/common-licenses/LGPL-2.1".

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
     http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache License 2.0 can
 be found in "/usr/share/common-licenses/Apache-2.0"

